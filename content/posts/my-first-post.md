---
title: "My First Post"
date: 2022-04-15T19:21:05-04:00
draft: false
---
# Welcome
Glad to see you here. Now get out. 

## Ranking of nuts
| Ranking | Type of Nuts |
| ------  |  ----------- |
| 1       | Cashew
| 2       | Peanut
| 3       | Pistachio
| 4       | Almond
| 5       | Pecan
| 6       | Walnut
| 7       | Macadamia
| 8       | Brazilian
| 9       | Hazelnuts
